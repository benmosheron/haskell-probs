--Pre-requisites:
--1) install ghc
--2) install cabal, then `cabal install random` to get the random package

-- To compile and run:
-- $ ghc haskellProbs && ./haskellProbs

-- Can we build functions probabilistically?

-- Import system.random for random number generator
import System.Random (randomIO)
-- Import replicateM_ from Control.Monad, so we can repeat the test a bunch of times
import Control.Monad (replicateM_)

-- Function to generate a random True/False
randBool :: IO Bool
randBool = randomIO

-- In Haskell, functions can be composed using the . operator
-- We can use this to build a function based on a random factor.

-- Given a base function f (with type a -> b), and two functions g1 and g2 with type (b -> c),
-- return a new function which has equal probabality of being:
-- 1) g1 . f
-- 2) g2 . f
-- The types of f and g* guarantee that they will compose.
-- The resulting function is wrapped in the IO monad, due to reliance on a PRNG.
buildFunction :: (a -> b) -> (b -> c) -> (b -> c) -> IO (a -> c)
buildFunction f g1 g2 = buildFunctionM f (pure g1) (pure g2)
-- In reality, we wrap g1 and g2 in IOs an call buildFunctionM, which does the real work.

-- To compose random functions with other random functions, we must allow the passing of IO wrapped functions
buildFunctionM :: (a -> b) -> IO (b -> c) -> IO (b -> c) -> IO (a -> c)
buildFunctionM f g1 g2 = composeM g f
  where g = chooseM g1 g2

-- Given two IO things, return one of them at random
chooseM :: IO a -> IO a -> IO a
chooseM g1 g2 = randBool >>= (\b -> if b then g1 else g2 )

-- compose f and functor of g
composeM :: Functor m => m (b -> c) -> (a -> b) -> m (a -> c)
composeM g f = fmap (\g' -> g' . f) g

-- Let's test it out by building a function with a few different branches:
-- * 4 then
--     either + 1
--         or * 2 then
--                either + 1
--                    or + 10

-- So our final function will be one of
-- * 4 + 1 (p = 0.5)
-- * 4 * 2 + 1 (p = 0.25)
-- * 4 * 2 + 10 (p = 0.25)

-- We apply the funtion to the value 1, resulting in 5, 9 or 18. 
-- We can make use of the Applicative <*> function, we just need to lift the input.
test :: IO Double
test = randomFunction2 <*> pure 1
  where randomFunction1 = buildFunction (* 2) (+ 1) (+ 10)
        randomFunction2 = buildFunctionM (* 4) randomFunction1 $ pure (+ 1)


-- Writes the wrapped Double to the console and discards the value
output :: Show a => IO a -> IO ()
output dio = (fmap show dio) >>= putStrLn

-- Entry point.
-- 1) `output test` output the result of running the test function
-- 2) `replicateM_ n` repeats running and outputting the test n times
main :: IO()
main = replicateM_ 50 $ output test
